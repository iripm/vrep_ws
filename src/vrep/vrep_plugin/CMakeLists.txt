cmake_minimum_required(VERSION 2.8.3)
project(vrep_plugin)

find_package(catkin REQUIRED)
find_package(catkin REQUIRED COMPONENTS tf std_msgs sensor_msgs image_transport vrep_common)

catkin_package(
#  INCLUDE_DIRS 
#  LIBRARIES 
   CATKIN_DEPENDS tf std_msgs sensor_msgs image_transport vrep_common
#  DEPENDS 
)

include_directories(include)
include_directories(${catkin_INCLUDE_DIRS})

add_library(v_repExtRos src/vrep_plugin.cpp src/v_repLib.cpp src/ROS_server.cpp src/vrepSubscriber.cpp) 

target_link_libraries(v_repExtRos ${catkin_LIBRARIES})
target_link_libraries(v_repExtRos pthread)
target_link_libraries(v_repExtRos dl)

add_dependencies(v_repExtRos vrep_common_generate_messages_cpp)
