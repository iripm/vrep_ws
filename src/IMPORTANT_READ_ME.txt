The two stacks contain several symbolic links,
they should be correctly redirected before trying
to compile anything!

These are the files you should look for:

v_repConst.h
v_repLib.h
v_repLib.cpp
v_repTypes.h

Once the symbolic links have been created, initialize
the ros workspace in "catkin_ws/src" folder:

$ catkin_init_workspace

After that:

$ cd .. && catkin_make

Remember to substitute the libv_repExtRos.so plugin in 
the V-REP installation folder by a symbolic link pointing
to the recently created one in catkin devel folder: 
../catkin_ws/devel/lib/libv_repExtRos.so

It is also important to source the ../devel/setup.bash file
into your .bashrc to correctly overlay this workspace on top
of your environment. 

