cmake_minimum_required(VERSION 2.8.3)

project(ros_bubble_rob)

find_package(catkin REQUIRED)
find_package(catkin REQUIRED COMPONENTS std_msgs sensor_msgs image_transport vrep_common)

catkin_package(
#  INCLUDE_DIRS 
#  LIBRARIES 
   CATKIN_DEPENDS std_msgs sensor_msgs image_transport vrep_common
#  DEPENDS 
)

include_directories(include)
include_directories(${catkin_INCLUDE_DIRS})

add_executable(rosBubbleRob src/rosBubbleRob.cpp) 

target_link_libraries(rosBubbleRob ${catkin_LIBRARIES})
add_dependencies(rosBubbleRob vrep_common_generate_messages_cpp)
